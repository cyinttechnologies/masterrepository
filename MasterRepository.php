<?php
/*MasterRepoitory.php
MasterRepository class for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Repository;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\ORM\EntityRepository;
/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MasterRepository extends \Doctrine\ORM\EntityRepository implements ContainerAwareInterface
{
    private $container;
    protected $filter_property;
    protected $FactoryType;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function findByFilter($filter = null, $parentid = null, $page = null, $items_per_page = 10, $order_by = null, $direction = 'ASC')
    {
        $query = $this->createQueryBuilder('e');
        if(!empty($filter))
        {
            $query
                ->andWhere('e.' . $this->filter_property . ' like :filter')
                ->setParameter(':filter', "%$filter%");
        }        

        $query = $this->orderByMethod($query, $order_by, $direction);

        if(!empty($parentid))        
            $query = $this->prepareFilterByParent($query, $parentid);        

        $query = $this->customQueryExtension($query);
      
        if(!empty($page))
        {   
            $query->select('count(e.id)');
            $count = $query->getQuery()->getSingleScalarResult();

            $skip = $this->calculateSkip($page, $items_per_page);
            $query->select('e');
            $query->setMaxResults($items_per_page);
            $query->setFirstResult($skip);      

            $result = $query->getQuery()->getResult();
            return ['count'=>$count, 'result'=>$result];
        }

        return $query->getQuery()->getResult();
    }

    public function getFactory($Doctrine, $Container)
    {
        return new $this->FactoryType($this, $Doctrine,$Doctrine->getManager());
    }

    public function authenticateWordPress($username, $password)
    {
        
    }

    public function customQueryExtension($query)
    {
        return $query;
    }


    public function orderByMethod($query, $order_by, $direction)
    {
        if(!empty($order_by))        
            $query->orderBy($order_by, $direction);       

        return $query;
    }

    public function prepareFilterByParent($query, $parentid)
    {
        return $query;
    }

    private function calculateSkip($page, $items_per_page)
    {
        $skip = ($page - 1) * $items_per_page;
        return $skip;
    }

}
